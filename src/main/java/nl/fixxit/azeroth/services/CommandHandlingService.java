package nl.fixxit.azeroth.services;

import nl.fixxit.azeroth.commands.MessageCreateCommand;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class CommandHandlingService {

    private CommandGateway commandGateway;

    public CommandHandlingService(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    public CompletableFuture<Object> handleMessageCreateCommand(MessageCreateCommand messageCreateCommand) {
        return commandGateway.send(messageCreateCommand);

    }
}
