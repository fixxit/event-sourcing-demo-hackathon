package nl.fixxit.azeroth.services;

import nl.fixxit.azeroth.domain.Message;
import nl.fixxit.azeroth.events.MessageCreatedEvent;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QueryHandlingService {

    private EventStore eventStore;

    public QueryHandlingService(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    public List<Message> handleMessageQuery(String id) {

        List<Message> messageEventHistory = new ArrayList<>();

        this.eventStore.readEvents(id).asStream().forEach(message -> {
            System.out.println("event: " + message.getPayload().toString());

            if (message.getPayloadType() == MessageCreatedEvent.class) {
                messageEventHistory.add(((MessageCreatedEvent) message.getPayload()).getMessage());
            }
        });

        return messageEventHistory;
    }

    private Message getMostRecentStateObject(ArrayList<Message> messageEventHistory) {
        if (messageEventHistory.size() == 0) {
            return new Message();
        }
        return messageEventHistory.get(messageEventHistory.size() - 1);
    }
}
