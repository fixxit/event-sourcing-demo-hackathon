package nl.fixxit.azeroth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzerothMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AzerothMessagingApplication.class, args);
	}
}

