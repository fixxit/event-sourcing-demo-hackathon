package nl.fixxit.azeroth.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.fixxit.azeroth.domain.Message;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageCreatedEvent {

    private String id;
    private Message message;
}
