package nl.fixxit.azeroth.config;

import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore;
import org.axonframework.eventsourcing.eventstore.inmemory.InMemoryEventStorageEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfig {

    @Bean
    public EmbeddedEventStore eventStorageEngine()
    {
        return new EmbeddedEventStore(new InMemoryEventStorageEngine());
    }
}
