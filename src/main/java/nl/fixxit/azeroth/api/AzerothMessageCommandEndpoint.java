package nl.fixxit.azeroth.api;

import nl.fixxit.azeroth.commands.MessageCreateCommand;
import nl.fixxit.azeroth.domain.Message;
import nl.fixxit.azeroth.services.CommandHandlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/command/message")
public class AzerothMessageCommandEndpoint {

    private CommandHandlingService commandHandlingService;
    private final String AZEROTH_MESSAGE_BOARD = "azeroth_message_board";

    @Autowired
    public AzerothMessageCommandEndpoint(CommandHandlingService commandHandlingService) {
        this.commandHandlingService = commandHandlingService;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public CompletableFuture<Object> messageCreateCommand(@RequestBody Message message) {
        return commandHandlingService.handleMessageCreateCommand(MessageCreateCommand.builder()
                .id(AZEROTH_MESSAGE_BOARD)
                .message(message)
                .build());
    }
}
