package nl.fixxit.azeroth.api;

import nl.fixxit.azeroth.domain.Message;
import nl.fixxit.azeroth.services.QueryHandlingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/query/message")
public class AzerothMessageQueryEndpoint {

    private QueryHandlingService queryHandlingService;
    private final String AZEROTH_MESSAGE_BOARD = "azeroth_message_board";

    @Autowired
    public AzerothMessageQueryEndpoint(QueryHandlingService queryHandlingService) {
        this.queryHandlingService = queryHandlingService;
    }

    @GetMapping
    @CrossOrigin
    public List<Message> userQuery() {
        return this.queryHandlingService.handleMessageQuery(AZEROTH_MESSAGE_BOARD);
    }
}
