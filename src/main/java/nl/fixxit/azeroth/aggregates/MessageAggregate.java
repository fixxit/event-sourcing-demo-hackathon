package nl.fixxit.azeroth.aggregates;

import nl.fixxit.azeroth.commands.MessageCreateCommand;
import nl.fixxit.azeroth.domain.Message;
import nl.fixxit.azeroth.events.MessageCreatedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.common.Assert;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
public class MessageAggregate {

    @AggregateIdentifier
    private String id;

    private Message message;

    public MessageAggregate() {
    }

    @CommandHandler
    public MessageAggregate(MessageCreateCommand command) {
        this.id = command.getId();

        Assert.notNull(command.getMessage().getLabel(), () -> "Label is required");

        apply(new MessageCreatedEvent(command.getId(), command.getMessage()));
    }

    @EventHandler
    public void on(MessageCreatedEvent event) {
        this.id = event.getId();
        this.message = event.getMessage();
    }
}