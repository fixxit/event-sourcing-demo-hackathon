package nl.fixxit.azeroth.commands;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import nl.fixxit.azeroth.domain.Message;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

@AllArgsConstructor
@Data
@Builder
public class MessageCreateCommand {

    @TargetAggregateIdentifier
    private final String id;
    private final Message message;
}
